package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import noobbot.actors.HelloWorldActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.gson.Gson;
import noobbot.actors.SocketActor;
import noobbot.actors.messages.*;
import noobbot.actors.track.*;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private final String carName;

    final ActorSystem helloActorSystem = ActorSystem.create("HelloActorSystem");
    final ActorRef master;
    final ActorRef socketActor;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        master = helloActorSystem.actorOf(HelloWorldActor.props(), "master");
        socketActor = helloActorSystem.actorOf(SocketActor.props(writer), "socketActor");
        carName = join.name;
        send(join);

        while((line = reader.readLine()) != null) {
            System.out.println(line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {

                JsonArray cars = gson.toJsonTree(msgFromServer.data).getAsJsonArray();
                for (int i = 0; i < cars.size(); i++) {
                    JsonObject current = cars.get(i).getAsJsonObject();
                    String name = current.get("id").getAsJsonObject().get("name").getAsString();
                    if(name.equals(carName)) {
                        double angle = current.get("angle").getAsDouble();
                        int index = current.get("piecePosition").getAsJsonObject().get("pieceIndex").getAsInt();
                        BigDecimal inPiecePosition;
                        if (current.get("piecePosition").getAsJsonObject().has("inPiecePosition")) {
                            inPiecePosition = current.get("piecePosition").getAsJsonObject().get("inPiecePosition").getAsBigDecimal();
                        } else {
                            inPiecePosition = new BigDecimal("0.0");
                        }
                        master.tell(new PieceStatus(angle, index, inPiecePosition), ActorRef.noSender());
                        //break;
                    }
                }
                //master.tell(msgFromServer, ActorRef.noSender());
                //send(new Throttle(0.5));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                JsonObject race = gson.toJsonTree(msgFromServer.data).getAsJsonObject();
                JsonArray array = race.getAsJsonObject("race").getAsJsonObject("track").getAsJsonArray("pieces");
                final String trackid = race.getAsJsonObject("race").getAsJsonObject("track").get("id").getAsString();
                List<Piece> pieces = new ArrayList<Piece>();
                for (int i = 0; i < array.size(); i++) {
                    JsonObject current = array.get(i).getAsJsonObject();
                    boolean hasSwitch = false;
                    if (current.has("switch")) {
                        hasSwitch = true;
                    }
                    if(current.has("length")) {
                        Straight straight = new Straight(hasSwitch, current.get("length").getAsDouble());
                        pieces.add(straight);
                    } else {
                        Bench bench = new Bench(hasSwitch, current.get("angle").getAsDouble(), current.get("radius").getAsDouble());
                        pieces.add(bench);
                    }
                }
                master.tell(new Track(trackid, pieces), ActorRef.noSender());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                System.out.println("Ping");
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

//abstract class SendMsg {
//    public String toJson() {
//        return new Gson().toJson(new MsgWrapper(this));
//    }
//
//    protected Object msgData() {
//        return this;
//    }
//
//    protected abstract String msgType();
//}
//
//class MsgWrapper {
//    public final String msgType;
//    public final Object data;
//
//    MsgWrapper(final String msgType, final Object data) {
//        this.msgType = msgType;
//        this.data = data;
//    }
//
//    public MsgWrapper(final SendMsg sendMsg) {
//        this(sendMsg.msgType(), sendMsg.msgData());
//    }
//}
//
//class Join extends SendMsg {
//    public final String name;
//    public final String key;
//
//    Join(final String name, final String key) {
//        this.name = name;
//        this.key = key;
//    }
//
//    @Override
//    protected String msgType() {
//        return "join";
//    }
//}
//
//class Ping extends SendMsg {
//    @Override
//    protected String msgType() {
//        return "ping";
//    }
//}
//
//class Throttle extends SendMsg {
//    private double value;
//
//    public Throttle(double value) {
//        this.value = value;
//    }
//
//    @Override
//    protected Object msgData() {
//        return value;
//    }
//
//    @Override
//    protected String msgType() {
//        return "throttle";
//    }
//}