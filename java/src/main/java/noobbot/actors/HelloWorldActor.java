package noobbot.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import akka.routing.RoundRobinPool;
import noobbot.actors.messages.MsgWrapper;
import noobbot.actors.messages.SendMsg;
import noobbot.actors.messages.Throttle;
import noobbot.actors.track.Piece;
import noobbot.actors.track.PieceStatus;
import noobbot.actors.track.Status;
import noobbot.actors.track.Track;

import java.io.PrintWriter;
import java.util.List;

public class HelloWorldActor extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    private String trackid;
    private List<Piece> pieces;
    private final ActorRef router;

    public HelloWorldActor() {
        this.router = getContext().actorOf(WorkerActor.props().withRouter(new RoundRobinPool(10)), "router");
        receive(ReceiveBuilder.
                match(PieceStatus.class, ps -> {
                    Piece current = pieces.get(ps.pieceIndex);
                    int prevIndex = ps.pieceIndex-1;
                    if(prevIndex < 0) {
                        prevIndex = pieces.size()-1;
                    }
                    Piece prev = pieces.get(prevIndex);
                    int nextIndex = ps.pieceIndex+1;
                    if(nextIndex >= pieces.size()) {
                        nextIndex = 0;
                    }
                    Piece next = pieces.get(nextIndex);

                    //send(new Throttle(0.5));
                    router.tell(new Status(current, prev, next), context().self());
                }).
                match(Track.class, track -> {
                    this.trackid = track.getId();
                    this.pieces = track.getPieces();
                }).
                matchAny(o -> log.info("received unknown message")).build()
        );
    }

    public static Props props() {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(HelloWorldActor.class, HelloWorldActor::new);
    }
}
