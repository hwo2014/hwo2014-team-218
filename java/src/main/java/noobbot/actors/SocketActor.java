package noobbot.actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import noobbot.actors.messages.SendMsg;
import noobbot.actors.messages.Throttle;
import noobbot.actors.track.Bench;
import noobbot.actors.track.Status;
import noobbot.actors.track.Straight;

import java.io.PrintWriter;

public class SocketActor extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(context().system(), this);
    private final PrintWriter writer;

    public SocketActor(final PrintWriter writer) {
        this.writer = writer;
        receive(ReceiveBuilder.
                match(SendMsg.class, this::send).
                matchAny(o -> log.info("received unknown message")).build()
        );
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    public static Props props(final PrintWriter writer) {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(SocketActor.class, () -> new SocketActor(writer));
    }
}
