package noobbot.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import noobbot.actors.messages.MsgWrapper;
import noobbot.actors.messages.SendMsg;
import noobbot.actors.messages.Throttle;
import noobbot.actors.track.*;

import java.io.PrintWriter;
import java.util.List;

public class WorkerActor extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    public WorkerActor() {
        receive(ReceiveBuilder.
                match(Status.class, status -> {
                    ActorSelection socket = context().actorSelection("/user/socketActor");

                    if(status.current instanceof Straight) {
                        if(status.next instanceof Bench) {
                            socket.tell(new Throttle(0.5), context().self());
                        } else {
                            socket.tell(new Throttle(0.8), context().self());
                        }
                    } else {
                        if (status.prev instanceof Straight) {
                            if(status.next instanceof Straight) {
                                socket.tell(new Throttle(0.6), context().self());
                            } else {
                                socket.tell(new Throttle(0.5), context().self());
                            }
                        } else {
                            if(status.next instanceof Straight) {
                                socket.tell(new Throttle(0.6), context().self());
                            } else {
                                socket.tell(new Throttle(0.5), context().self());
                            }
                        }
                    }
                }).
                matchAny(o -> log.info("received unknown message")).build()
        );
    }

    public static Props props() {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(WorkerActor.class, WorkerActor::new);
    }
}
