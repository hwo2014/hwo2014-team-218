package noobbot.actors.messages;

/**
 * Created by kjsaila on 24/04/14.
 */
public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}
