package noobbot.actors.messages;

import com.google.gson.Gson;

/**
 * Created by kjsaila on 24/04/14.
 */
public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
