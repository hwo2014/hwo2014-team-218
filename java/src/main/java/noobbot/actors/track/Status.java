package noobbot.actors.track;

import java.util.List;

/**
 * Created by kjsaila on 25/04/14.
 */

public class Status {
    public final Piece current;
    public final Piece prev;
    public final Piece next;

    public Status(final Piece current, final Piece prev, final Piece next) {
        this.current = current;
        this.prev = prev;
        this.next = next;
    }
}
