package noobbot.actors.track;

import java.util.List;

/**
 * Created by kjsaila on 25/04/14.
 */

public class Track {
    public final String id;
    public final List<Piece> pieces;

    public Track(final String id, final List<Piece> pieces) {
        this.id = id;
        this.pieces = pieces;
    }

    public String getId() {
        return id;
    }

    public List<Piece> getPieces() {
        return pieces;
    }
}
