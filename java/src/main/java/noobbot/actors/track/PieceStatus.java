package noobbot.actors.track;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kjsaila on 25/04/14.
 */

public class PieceStatus {
    public final double angle;
    public final int pieceIndex;
    public final BigDecimal inPieceDistance;


    public PieceStatus(final double angle, final int pieceIndex, final BigDecimal inPieceDistance) {
        this.angle = angle;
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
    }
}
