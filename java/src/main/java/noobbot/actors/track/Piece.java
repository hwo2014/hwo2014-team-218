package noobbot.actors.track;

import com.google.gson.Gson;
import noobbot.actors.messages.MsgWrapper;

/**
 * Created by kjsaila on 24/04/14.
 */
public abstract class Piece {
    private boolean hasSwitch;

    public boolean isHasSwitch() {
        return hasSwitch;
    }

    public void setHasSwitch(boolean hasSwitch) {
        this.hasSwitch = hasSwitch;
    }
}
