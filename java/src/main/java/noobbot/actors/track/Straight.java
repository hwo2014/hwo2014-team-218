package noobbot.actors.track;

import noobbot.actors.messages.SendMsg;

/**
 * Created by kjsaila on 24/04/14.
 */
public class Straight extends Piece {

    private double piecelength;

    public Straight(Boolean hasSwitch, double piecelength) {
        this.setHasSwitch(hasSwitch);
        this.piecelength = piecelength;
    }

    public double getPiecelength() {
        return piecelength;
    }

    public void setPiecelength(double piecelength) {
        this.piecelength = piecelength;
    }
}
