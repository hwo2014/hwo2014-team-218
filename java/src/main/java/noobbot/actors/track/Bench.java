package noobbot.actors.track;

/**
 * Created by kjsaila on 24/04/14.
 */
public class Bench extends Piece {

    private double angle;
    private double radius;

    public Bench(Boolean hasSwitch, double angle, double radius) {
        this.setHasSwitch(hasSwitch);
        this.angle = angle;
        this.radius = radius;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
