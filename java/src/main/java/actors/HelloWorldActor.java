package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;

public class HelloWorldActor extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    public HelloWorldActor() {
        receive(ReceiveBuilder.
                match(String.class, s -> {
                    log.info("Received String message: {}", s);
                }).
                matchAny(o -> log.info("received unknown message")).build()
        );
    }

    static Props props() {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(HelloWorldActor.class, () -> new HelloWorldActor());
    }
}
